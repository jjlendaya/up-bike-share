var gulp = require('gulp');
var webserver = require('gulp-webserver');
var angularFileSort = require('gulp-angular-filesort');
var inject = require('gulp-inject');
var debug = require('gulp-debug');

var paths = {
    css: [
        './src/css/*.css'
    ],
    js: [
        './src/app/**/*.js'
    ]
};

gulp.task('inject', function() {
    return gulp.src('./src/index.html')
        .pipe(inject(gulp.src(paths.css, {read: false}, {relative: true}), {ignorePath: 'src/', addRootSlash: false}))
        .pipe(inject(gulp.src(paths.js, {read: true}, {relative: true}).pipe(angularFileSort()).pipe(debug()),
            {ignorePath: 'src/', addRootSlash: false}
        ))
    .pipe(gulp.dest('./src'));
});

gulp.task('serve', ['inject'], function() {
    gulp.src('./')
    .pipe(webserver({
        livereload: true,
        open: 'http://localhost:8000/src'
    }));
});