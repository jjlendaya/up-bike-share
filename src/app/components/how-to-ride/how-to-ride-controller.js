(function() {
    angular.module('upBikeShareMain').controller('HowToRidePageController', ['$scope', 'GOOGLE_MAP_STYLE', 'StationService', '$uibModal', 'anchorSmoothScroll', '$location', function($scope, GOOGLE_MAP_STYLE, StationService, $uibModal, anchorSmoothScroll, $location) {
        $scope.stations = StationService.getMarkerArray();
        $scope.selected = null;
        $scope.displaySidebar = false;
        
        $scope.openSidebar = function(id) {
            $scope.selected = $scope.stations[parseInt(id)].id;
            $scope.displaySidebar = true;
        }
        
        $scope.closeSidebar = function() {
            $scope.displaySidebar = false;
        }
        
        $scope.openOrientationModal = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/components/about-us/orientation-modal-template.html',
                controller: 'OrientationModalController',
                size: 'lg'
            });
        }
        
        $scope.scrollTo = function(id) {
            $location.hash(id);
            anchorSmoothScroll.scrollTo(id);
        }
    }]);
})();