(function() {
    angular.module('upBikeShareMain').controller('AboutUsPageController', ['$scope', '$anchorScroll', '$location', 'anchorSmoothScroll', function($scope, $anchorScroll, $location, anchorSmoothScroll) {
        $scope.scrollTo = function(id) {
            $location.hash(id);
            anchorSmoothScroll.scrollTo(id);
        }
    }]);
})();