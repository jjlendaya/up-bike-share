(function() {
    angular.module('upBikeShareMain')
    .controller('OrientationModalController', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
        
        $scope.close = function() {
            $uibModalInstance.close();
        };
    }]);
})();