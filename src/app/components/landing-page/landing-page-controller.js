(function() {
    angular.module('upBikeShareMain').controller('LandingPageController', ['$scope', 'uiGmapGoogleMapApi', 'GOOGLE_MAP_STYLE', 'StationService', function($scope, uiGmapGoogleMapApi, GOOGLE_MAP_STYLE, StationService) {
        uiGmapGoogleMapApi.then(function(maps) {
            function openInfoWindow(marker, eventName, model) {
                $scope.map.window.show = true;
                $scope.map.window.model = model.coords;
                $scope.map.window.content.name = model.name;
                $scope.map.window.content.description = model.description.substring(0, 100);
            }
            $scope.map = {
                center: {
                    latitude: 14.655266,
                    longitude: 121.066273
                },
                zoom: 16,
                markers: [],
                markersEvents: {
                    click: openInfoWindow
                },
                window: {
                    marker: {},
                    show: false,
                    closeClick: function() {
                        this.show = false;
                    },
                    options: {
                        disableAutoPan: true
                    },
                    content: {
                        name: null,
                        description: null
                    }
                },
                options: {
                    styles: GOOGLE_MAP_STYLE
                }
            };
            
            $scope.map.markers = StationService.getMarkerArray();
        });
        
    }]);
})();