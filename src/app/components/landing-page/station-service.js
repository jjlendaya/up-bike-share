(function() {
    angular.module('upBikeShareMain').factory('StationService', function () {
        var getMarkerArray = function() {
            var markers = [
                //Ilang-ilang
                {
                    name: "Ilang-ilang",
                    coords: {
                        latitude: 14.659304,
                        longitude: 121.073066
                    },
                    description: "Ilang-Ilang was established on January 1949 in UP Diliman. It was the first-ever UP dormitory that is exclusive for girls. This dorm has a very unique architectural structure with its four wings that form a rectangle, enclosing an inner lawn. All of the rooms face this lawn, which has a beautiful fountain and a lot of Caimito trees.",
                    thumbnail: "assets/img/Stations/ilang-edit.jpg"
                    
                },
                //Kalayaan
                {
                    name: "Kalayaan",
                    coords: {
                        latitude: 14.658813,
                        longitude: 121.068804
                    },
                    description: "The Kalayaan Residence Hall is a university-operated dormitory located inside the UP Diliman campus. It is a co-educational dormitory which houses freshmen. It is located at the corner blocks of Roces and Laurel Avenues. It is currently managed by Mr. Rodolfo Robidillo. The Office of Student Housing is located inside the dormitory.",
                    thumbnail: "assets/img/Stations/kalay-edit.jpg"
                },
                //Molave
                {
                    name: "Molave",
                    coords: {
                        latitude: 14.65795,
                        longitude: 121.067924
                    },
                    description: "The Molave Dormitory is a co-educational dormitory that accommodates upperclassmen. This dormitory prioritizes sophomores who usually come from the Kalayaan Residence Hall. It has a capacity of 380 residents with rooms of varying capacity from two to four residents.",
                    thumbnail: "assets/img/Stations/molave-edit.jpg"
                },
                //Yakal
                {
                    name: "Yakal",
                    coords: {
                        latitude: 14.657768,
                        longitude: 121.069198
                    },
                    description: "Yakal Dormitory is a co-educational dorm located along R. Magsaysay st., at the back of the College of Engineering. It has an overall capacity of 378 residents. There are slots for 196 males and 182 females in this dormitory. It was built in 1962 from the remnants of the demolished UP Men's North Dormitory. The first residents of this dormitory are US Peace Corps-male and female volunteers and their Filipino equivalent. Later on, it accommodated students who were not accepted at Ilang-Ilang.",
                    thumbnail: "assets/img/Stations/yakal-edit.jpg"
                },
                //Ipil
                {
                    name: "Ipil",
                    coords: {
                        latitude: 14.657825,
                        longitude: 121.069898
                    },
                    description: "Ipil Residence Hall is a two-storey, co-educational graduate students dormitory facing the UP Computer Center. It is adjacent to Yakal Dormitory and is just a stone's throw away from the College of Engineering and the College of Social Work and Community Development. ",
                    thumbnail: "assets/img/Stations/ipil-edit.jpg"
                },
                //Acacia
                {
                    name: "Acacia",
                    coords: {
                        latitude: 14.65925,
                        longitude: 121.070485
                    },
                    description: "1)    The Acacia dormitory is a new coed dormitory that caters to all students. It was constructed to commemorate the 100th year of the college of law. It is located close to the UP chapel and the shopping center.",
                    thumbnail: "assets/img/Stations/acacia-edit.jpg"
                },
                //Kamia
                {
                    name: "Kamia",
                    coords: {
                        latitude: 14.652077,
                        longitude: 121.07044
                    },
                    description: "Kamia Residence Hall was established in 1961 as a dormitory for upperclassmen. It is beside another dormitory called the Sampaguita Residence Hall. The two dormitories are connected by a covered walk. The dormitory has a basement, first floor, and second floor. Every floor is equipped with a communal toilet and a utility room. There is also a piano, a television, an intercom system and a public telephone.",
                    thumbnail: "assets/img/Stations/kamia-edit.jpg"
                },
                //Sampaguita
                {
                    name: "Sampaguita",
                    coords: {
                        latitude: 14.652143,
                        longitude: 121.070994
                    },
                    description: "Sampaguita Residence Hall is situated along Quirino Avenue across the Zoology building and adjacent to the Kamia Residence Hall. The two-story dormitory exclusively houses female upperclassmen of UP-Diliman. Rooms are adequately furnished for 284 residents, with four students sharing a room. At the reception hall, there is a television set, a stereo and a piano for the residents. Two refrigerators are also available for use.",
                    thumbnail: "assets/img/Stations/sampa-edit.jpg"
                },
                //Centennial
                {
                    name: "Centennial",
                    coords: {
                        latitude: 14.647835,
                        longitude: 121.062483
                    },
                    description: "The centennial dormitory is a relatively new coed dormitory that used to cater only to engineering students. It now caters to all students. It was constructed to commemorate the 100th year of the college of engineering. It is located along the road of the college of fine arts near the exit that goes into CP Garcia.",
                    thumbnail: "assets/img/Stations/cente-edit.jpg"
                },
                //Math
                {
                    name: "Math",
                    coords: {
                        latitude: 14.648206,
                        longitude: 121.071159
                    },
                    description: "The Institute of Mathematics is the leading institution for mathematics research and education in the Philippines. Since 1998, it has been recognized by the Philippine Commission on Higher Education as a Center of Excellence. It is home to the country's best and more promising researchers in mathematics.",
                    thumbnail: "assets/img/Stations/math-edit.jpg"
                },
                //Engg
                {
                    name: "Engg",
                    coords: {
                        latitude: 14.656355,
                        longitude: 121.069509
                    },
                    description: "The College of Engineering of the University of the Philippines is the largest degree-granting unit in the UP Diliman campus in terms of student population. The college is also known formally as UP COE, COE, and informally as Eng'g (pronounced \"eng\").",
                    thumbnail: "assets/img/Stations/melchor-edit.jpg"
                },
                //AS
                {
                    name: "AS",
                    coords: {
                        latitude: 14.653418,
                        longitude: 121.068724
                    },
                    description: "Palma Hall (more commonly known today as \"AS\" to the UP Community) is located on Roxas Avenue along the Academic Oval of the University of the Philippines Diliman, Quezon City. As one of the first buildings of U.P. Diliman since the approval of the transfer of U.P. Manila to Diliman, the responsibility of building Palma Hall fell on the shoulders of the first campus architect, Cesar Homero Rosales Concio. Palma Hall is also where most of the courses of the Revitalized General Education Program.",
                    thumbnail: "assets/img/Stations/as-edit.jpg"
                },
                //Main lib
                {
                    name: "Main Lib",
                    coords: {
                        latitude: 14.654583,
                        longitude: 121.070834
                    },
                    description: "The University Library of UP Diliman encompasses one Main Library and more than 40 College/Unit Libraries. It envisions global information exchange throughout the UP Library System. It is mandated to be the information resource center of excellence in the social sciences, humanities and basic sciences. It strives for full automation, a world-class collection and a staff of thoroughly modern information professionals.",
                    thumbnail: "assets/img/Stations/main-lib-edit.jpg"
                },
                //Arki
                {
                    name: "Arki",
                    coords: {
                        latitude: 14.651853,
                        longitude: 121.065084
                    },
                    description: "The UP College or Architecture (UPCA), commonly known as Archi, is one of degree-granting units of University of the Philippines Diliman. Located along E. delos Santos St. in Diliman campus, it currently offers two undergraduate programs and two masters program.",
                    thumbnail: "assets/img/Stations/arki-edit.jpg"
                },
                //CHK
                {
                    name: "CHK",
                    coords: {
                        latitude: 14.658897,
                        longitude: 121.06302
                    },
                    description: "From the Department of Physical Education, the need to institute programs to maximize the growth and development of physical education and sports in the country was conceptualized under the leadership of Dr. Aparicio H. Mequi, who was then the director of the Department. Thus, this led to the establishment of the UP Institute of Sports, Physical Education, and Recreation (SPEAR) in 1976.",
                    thumbnail: "assets/img/Stations/chk-edit.jpg"
                },
                //CMC
                {
                    name: "CMC",
                    coords: {
                        latitude: 14.656732,
                        longitude: 121.064675
                    },
                    description: "The UP College of Mass Communication or UP CMC is an institution in mass media education in the Philippines. Formally established in March 1965 as the Institute of Mass Communication, it is an independent degree-granting unit of the University of the Philippines Diliman, and offers programs in the field of media and communication studies.. UP  CMC  exists  primarily  to  inculcate  and  foster  awareness  of  the ethical and social significance of, as well as responsibility in, the use of interpersonal and mass media including the press, radio, television, film, and new technological resources.",
                    thumbnail: "assets/img/Stations/cmc-edit.jpg"
                },
                //Econ
                {
                    name: "Econ",
                    coords: {
                        latitude: 14.655687,
                        longitude: 121.073203
                    },
                    description: "The University of the Philippines School of Economics (UPSE), located at the UP campus in Diliman, was established in 1965 and offers academic instruction leading to baccalaureate and master's degrees and the PhD in economics. The School is the only institution in the country with an active and internationally recognized Ph.D. program in economics; a high ratio of full-time faculty with Ph.D.s; and a nationally unparalleled record of international publications in the discipline.",
                    thumbnail: "assets/img/Stations/econ-edit.jpg"
                },
                //MBB
                {
                    name: "MBB",
                    coords: {
                        latitude: 14.650557,
                        longitude: 121.071687
                    },
                    description: "The Molecular Biology and Biotechnology Program (MBBP) was established by the University of the Philippines Board of Regents in 1987 under the College of Science, U.P. Diliman. This interdisciplinary program was created to train undergraduate and graduate students in Molecular Biology and Biotechnology to respond to the need of the Philippines for molecular biologists and biotechnologists in academe and the industry.",
                    thumbnail: "assets/img/Stations/mbb-edit.jpg"
                },
                //DCS
                {
                    name: "DCS",
                    coords: {
                        latitude: 14.648984,
                        longitude: 121.068715
                    },
                    description: "The Department of Computer Science is one of nine departments in the University of the Philippines Diliman College of Engineering. The bachelor of science in computer science program is designed to equip the student with knowledge of the fundamental concepts and a reasonable mastery of the basic tools and techniques in computer science. The master of science in computer science program aims to provide the student with both breadth and depth of knowledge in the concepts and techniques related to the design, programming, and application of computing systems.",
                    thumbnail: "assets/img/Stations/dcs-edit.jpg"
                }
            ]

            for (var i = 0; i < markers.length; i++) {
                markers[i].id = i;
                markers[i].show = false;
                markers[i].options = {
                    disableAutoPan: true,
                    content: "Hello"
                }
                markers[i].closeClick = function() {
                    this.show = false;
                }
            }

            return markers;
        };
        
        return {
            getMarkerArray: getMarkerArray
        };
    });
})();