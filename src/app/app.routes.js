(function () {
    angular.module('upBikeShareMain').config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('root', {
            url: '/',
            views: {
                'main-content': {
                    templateUrl: 'app/components/landing-page/landing-page-template.html',
                    controller: 'LandingPageController'
                }
            }
        })
            .state('how-to-ride', {
            url: '/how-to-ride',
            views: {
                'main-content': {
                    templateUrl: 'app/components/how-to-ride/how-to-ride.html',
                    controller: 'HowToRidePageController'
                }
            }
        })
            .state('about-us', {
            url: '/about-us',
            views: {
                'main-content': {
                    templateUrl: 'app/components/about-us/about-us.html',
                    controller: 'AboutUsPageController'
                }
            }
        });
    });
})();
