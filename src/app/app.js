(function() {
    angular
    .module('upBikeShareMain', ['ui.bootstrap', 'ui.router', 'uiGmapgoogle-maps', 'ngAnimate'])
    .config(function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyC0hW623Fc648n002fzoVhX7MoUBcSJ8Bo',
            libraries: 'weather, geometry, visualization'    
        });
    })
    .run(function($rootScope, $location, $anchorScroll) {
        $rootScope.$on('$stateChangeSuccess', function() {
            angular.element(document)[0].body.scrollTop = angular.element(document)[0].documentElement.scrollTop = 0;
        });
        
        $rootScope.$on('$routeChangeSuccess', function() {
            if ($location.hash()) {
                $anchorScroll();
            }
        })
    });
    
})();